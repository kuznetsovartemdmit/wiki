package ru.qwep.qwepwiki;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@Slf4j
public class WikiProtoController {
    @GetMapping("/back/getArticles")
    @CrossOrigin(origins = "http://localhost:8080")
    public List<Article> getArticle() {
        log.info("запрос был в :" + LocalDateTime.now());
        return List.of(
                new Article("Статья 1", "www.hui.prigozhin"),
                new Article("Статья 2", "www.zhopa.prigozhin"),
                new Article("Статья 3", "www.vagina.prigozhin")
        );
    }
}
