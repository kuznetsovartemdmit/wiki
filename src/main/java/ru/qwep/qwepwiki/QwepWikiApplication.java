package ru.qwep.qwepwiki;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QwepWikiApplication {

	public static void main(String[] args) {
		SpringApplication.run(QwepWikiApplication.class, args);
	}

}
