# First stage: complete build environment
FROM maven:3.8.3-openjdk-17 AS builder
ADD ./ ./
RUN mvn clean package


From openjdk:17-jdk-slim
COPY --from=builder /target/qwep-wiki-0.0.1-SNAPSHOT.jar qwep-wiki-0.0.1-SNAPSHOT.jar
CMD ["java", "-jar", "qwep-wiki-0.0.1-SNAPSHOT.jar"]